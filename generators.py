import random


def dni():
    dni = random.randint(10000000, 49999999)
    letters = "TRWAGMYFPDXBNJZSQVHLCKE"
    module = dni % 23
    return "{}{}".format(str(dni), letters[module])


def iban(bank):
    IBANS = {
        "caixabank": "ES6621000418401234567891",
        "santander": "ES6000491500051234567892"
    }
    assert bank in IBANS
    return IBANS[bank]