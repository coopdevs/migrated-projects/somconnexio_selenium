from faker import Faker
from browser import setup_browser
import forms
from generators import iban

faker = Faker()


def create_mobile_contract(url, bank, dni, phone, email):
    browser = setup_browser()

    browser.get(url)

    fake_name = faker.first_name()
    fake_surname = faker.last_name()
    fake_lastname = faker.last_name()
    fake_iban = iban(bank)

    new_contract_step1_form = forms.NewContractStep1Form(
        browser=browser,
        new_customer=True,
        dni=dni,
        name=fake_name,
        surname=fake_surname,
        lastname=fake_lastname,
        phone=phone,
        email=email,
    )
    new_contract_step1_form.fill()
    new_contract_step1_form.next_step()

    new_contract_step2_form = forms.NewMobileContractStep2Form(browser=browser)
    new_contract_step2_form.fill()
    new_contract_step2_form.next_step()

    new_contract_step3_form = forms.NewMobileContractStep3Form(browser=browser)
    new_contract_step3_form.fill()
    new_contract_step3_form.next_step()

    new_contract_step4_form = forms.NewContractStep4Form(
        browser=browser,
        iban=fake_iban
    )
    new_contract_step4_form.fill()
    new_contract_step4_form.confirm()

    fake_name = "{} {} {}".format(fake_name, fake_surname, fake_lastname)
    fake_email = email

    print("---")
    print("New contract created!\n")
    print("{}".format(fake_name))
    print("{}".format(fake_email))
    print("{}".format(fake_iban))

    browser.close()


def create_adsl_contract(url, bank, dni, phone, email):
    browser = setup_browser()

    browser.get(url)

    fake_name = faker.first_name()
    fake_surname = faker.last_name()
    fake_lastname = faker.last_name()
    fake_iban = iban(bank)

    new_contract_step1_form = forms.NewContractStep1Form(
        browser=browser,
        new_customer=True,
        dni=dni,
        name=fake_name,
        surname=fake_surname,
        lastname=fake_lastname,
        phone=phone,
        email=email,
    )
    new_contract_step1_form.fill()
    new_contract_step1_form.next_step()
    customer_data = new_contract_step1_form.data

    new_contract_step2_form = forms.NewADSLContractStep2Form(browser=browser)
    new_contract_step2_form.fill()
    new_contract_step2_form.next_step()

    new_contract_step3_form = forms.NewADSLContractStep3Form(browser=browser)
    new_contract_step3_form.fill(**customer_data)

    new_contract_step3_form.next_step()

    new_contract_step4_form = forms.NewContractStep4Form(
        browser=browser,
        iban=fake_iban
    )
    new_contract_step4_form.fill()
    new_contract_step4_form.confirm()

    fake_name = "{} {} {}".format(fake_name, fake_surname, fake_lastname)
    fake_email = email

    print("---")
    print("New contract created!\n")
    print("{}".format(fake_name))
    print("{}".format(fake_email))
    print("{}".format(fake_iban))

    browser.close()


