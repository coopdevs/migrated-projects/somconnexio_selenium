# SomConnexio Selenium Suite

# Requirements

 * python3.x
 * virtualenvwrapper (optional)

## Setup

Download a Chrome [webdriver](https://chromedriver.storage.googleapis.com/index.html). You should download the same version as the chrome browser installed in your machine.

```commandline
$ wget https://chromedriver.storage.googleapis.com/70.0.3538.67/chromedriver_linux64.zip
$ unzip chromedriver_linux64.zip
$ mv chromedriver <directory-in-your-PATH>
```

You might want to move the `chromedriver` binary to `~/.pyenv/shims/` which is already listed in your `$PATH`.

You can create a virtualenv to install required python packages there:

### Virtualenv

* Pyenv
```commandline
$ pyenv virtualenv <python3-version> somconnexio_selenium
```

### Install requirements
```commandline
$ pip install -r requirements.txt
```

## Run

```commandline
$ python sc-web.py
```

You can customize the script behaviour. See:

```commandline
$ python sc-web.py --help
```
