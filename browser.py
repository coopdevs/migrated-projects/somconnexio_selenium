from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options


def setup_browser(headless=False):
    opts = Options()
    if headless:
        opts.set_headless()
        assert opts.headless  # Operating in headless mode

    opts.add_argument("--start-maximized")
    return Chrome(options=opts)
